var positionController = require("../controllers/position-controller")

module.exports = function(app) {

    app.route("/positions").get(positionController.listPossibilities);

};