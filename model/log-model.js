var mongoose = require("mongoose");

var Schema = mongoose.Schema;

var Log = new Schema({
    date: {
        type: Date,
        default: Date.now
    },
    positions: "array"
});

module.exports = mongoose.model("Logs", Log);