var express = require("express");
var bodyParser = require("body-parser");
var mongoose = require("mongoose");

var LogModel = require("./model/log-model");
var routes = require("./routes/routes");

var app = express();
var port = process.env.PORT || 3000;

mongoose.Promise = global.Promise;
mongoose.connect("mongodb://localhost/chess");

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

routes(app);

app.listen(port);

console.log("Chess API running on " + port);